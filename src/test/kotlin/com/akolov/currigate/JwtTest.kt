package com.akolov.currigate

import org.junit.Assert.assertEquals
import org.junit.Test


class JwtTest {

    @Test
    fun testCreateVerify() {
        val jwt = Jwt("secret")
        val token = jwt.create(User("123", null, UserPreferencies()))
        val user = jwt.getUser(token)
        assertEquals(user?.id, "123")
    }
}