package com.akolov.currigate

import com.google.api.client.auth.oauth2.*
import com.google.api.client.http.*
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.GenericJson
import com.google.api.client.json.JsonObjectParser
import com.google.api.client.json.jackson.JacksonFactory
import com.google.api.client.util.IOUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Controller
open class LoginControler(val google: AuthClient,
                          val userService: UserService,
                          val identityFilter: IdentityFilter) {


    val log = LoggerFactory.getLogger(IdentityFilter::class.java)

    @RequestMapping(method = arrayOf(RequestMethod.POST), value = "/logout")
    open fun logout(req: HttpServletRequest, response: HttpServletResponse) {
        identityFilter.updateUserInRequest(req, response, userService.createNew())
    }

    @RequestMapping(method = arrayOf(RequestMethod.GET), value = "/login/google/{client}")
    open fun login(req: HttpServletRequest, @PathVariable("client") client: String, response: HttpServletResponse) {

        val decoded = String(Base64.getDecoder().decode(client))
        val url = AuthorizationRequestUrl(google.userAuthorizationUri, google.clientId,
                Arrays.asList("code"))
                .setScopes(google.scope)
                .setRedirectUri(decoded + "/login/google/" + client).build()
        response.sendRedirect(url)
    }


    @RequestMapping(method = arrayOf(RequestMethod.GET), value = "/login/google/{client}", params = arrayOf("code"))
    open fun loginCallback(req: HttpServletRequest, resp: HttpServletResponse, @PathVariable("client") client: String): ResponseEntity<User> {

        val decoded = String(Base64.getDecoder().decode(client))

        val authResponse = AuthorizationCodeResponseUrl(rebuildUrl(req, true));
        if (authResponse.getError() != null) {
            return ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        try {
            val response = AuthorizationCodeTokenRequest(NetHttpTransport(), JacksonFactory(),
                    GenericUrl(google.accessTokenUri), authResponse.getCode())
                    .setRedirectUri(decoded + "/login/google/" + client)
                    .setClientAuthentication(
                            BasicAuthentication(google.clientId, google.clientSecret))
                    .execute();
            val credential = Credential(BearerToken.authorizationHeaderAccessMethod())
            credential.setFromTokenResponse(response)
            val userInfo = getUserInfo(credential.accessToken)
            val currentUser = req.getAttribute(IdentityFilter.ATTR_USER) as User
            val user = findOrCreateUser(currentUser, userInfo!!)
            identityFilter.updateUserInRequest(req, resp, user)
            val headers = org.springframework.http.HttpHeaders()
            headers.add("Location", "/")
            return ResponseEntity(headers, HttpStatus.FOUND)
        } catch (e: TokenResponseException) {
            log.error("Error",e)
            return ResponseEntity(HttpStatus.UNAUTHORIZED)
        }
    }


    private fun findOrCreateUser(currentUser: User, userInfo: GenericJson): User {
        val user = userService.findByIdentity(userInfo.get("sub") as String)
        if (user != null) {
            log.debug("Found user with identity [" + userInfo.get("sub") + "]")
            return user
        } else {
            log.debug("Did not find user with identity [" + userInfo.get("sub")
                    + "], creating from userInfo " +userInfo)
            val newIdentity = Identity(
                    sub = userInfo.get("sub") as String,
                    name = userInfo.get("name") as String,
                    givenName = userInfo.get("given_name") as String,
                    familyName = userInfo.get("family_name") as String,
                    profile = userInfo.get("profile") as String,
                    picture = userInfo.get("picture") as String,
                    email = userInfo.get("email") as String,
                    gender = userInfo.get("gender") as String,
                    locale = userInfo.get("locale") as String
            )
            return userService.register(currentUser.id, newIdentity)
        }
    }

    private fun getUserInfo(accessToken: String): GenericJson? {
        val jsonFactory = JacksonFactory()
        val requestFactory = NetHttpTransport().createRequestFactory()
        val request = requestFactory.buildGetRequest(GenericUrl(google.userInfoUri))
        request.setParser(JsonObjectParser(jsonFactory))
        request.setThrowExceptionOnExecuteError(false)
        val headers = HttpHeaders()
        headers.setAuthorization("Bearer " + accessToken)
        request.setHeaders(headers)
        val userInfoResponse = request.execute()
        if (userInfoResponse.isSuccessStatusCode()) {
            return userInfoResponse.parseAs(GenericJson::class.java)
        } else {
            return null
        }
    }

    // Application port as seen from outside (for callback url)
    @Value("\${APPPORT:80}")
    var applicationPort: Int = 80

    fun rebuildUrl(req: HttpServletRequest, withParams: Boolean): String {
        var result = if (req.isSecure) "https" else "http"
        result += "://" + req.serverName
        if (applicationPort != 80) {
            result += ":" + applicationPort
        }
        result += req.servletPath

        if (withParams) {
            if (req.queryString != null && req.queryString.length > 0) {
                result += "?" + req.queryString;
            }
        }
        return result
    }
}


@Controller
open class RouterControler(val userService: UserService) {

    val HEADER_USER = "x-curri-user"
    val HEADER_APP = "x-curri-app"

    @RequestMapping("/service/{serviceName}/**")
    @ResponseBody
    open fun route(req: HttpServletRequest, resp: HttpServletResponse, @PathVariable serviceName: String): Any? {

        if (serviceName == "user-details") {
            val user = req.getAttribute(IdentityFilter.ATTR_USER) as User
            return userService.userDetails(user.id)
        }

        val requestFactory = NetHttpTransport().createRequestFactory()
        val serviceUrl = GenericUrl(serviceUrl(req, serviceName))
        var content : HttpContent? = null;
        if( req.method == "POST") {
            content = InputStreamContent(req.getHeader("Content-type"), req.getInputStream())
        }
        val request = requestFactory.buildRequest(req.method, serviceUrl, content)
        request.throwExceptionOnExecuteError = false

        val headers = HttpHeaders()
        val user = req.getAttribute(IdentityFilter.ATTR_USER) as User
        headers.set(HEADER_USER, user.id)
        headers.set(HEADER_APP, "curri")
        request.headers = headers

        val otherResponse = request.execute()

        resp.status = otherResponse.statusCode
        IOUtils.copy(otherResponse.content, resp.outputStream)
        return null
    }

    fun serviceUrl(req: HttpServletRequest, serviceName: String): String {

        val prefix = "/service/" + serviceName
        val suffix = req.servletPath.substring(prefix.length)

        val serviceHost = System.getenv().get("SERVICE_" + serviceName.toUpperCase())
                ?: "service-" + serviceName + ":9000"

        var result = "http://" + serviceHost + suffix
        if (req.queryString != null && req.queryString.length > 0) {
            result += "?" + req.queryString;
        }

        return result
    }
}

