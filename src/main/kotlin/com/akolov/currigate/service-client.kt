package com.akolov.currigate

import feign.FeignException
import org.springframework.cloud.netflix.feign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.*

interface UserService {
    fun userDetails(id: String): User?
    fun createNew(): User
    fun register(currentUserId: String, identity: Identity): User
    fun findByIdentity(sub: String): User?
}

@FeignClient("service-users")
interface UserServiceClient {
    @RequestMapping(
            method = arrayOf(RequestMethod.POST),
            value = "/user/create")
    fun createNew(@RequestHeader("X-CURRI-ROLES") roles: String): User

    @RequestMapping(
            method = arrayOf(RequestMethod.PUT),
            value = "/user/{currentUserId}/identity",
            consumes = arrayOf("application/json"))
    @ResponseBody
    fun register(@RequestHeader("X-CURRI-ROLES") roles: String,
                 @PathVariable("currentUserId") currentUserId:
                 String, @RequestBody identity: Identity): User

    @RequestMapping(method = arrayOf(RequestMethod.GET), value = "/user")
    fun findByIdentity(@RequestHeader("X-CURRI-ROLES") roles: String,
                       @RequestParam("identity") sub: String):
            User?

    @RequestMapping(method = arrayOf(RequestMethod.GET), value = "/user/details/{id}")
    fun userDetails(@RequestHeader("X-CURRI-ROLES") roles: String,
                    @PathVariable("id") id: String): User?

}


@Component
class UserServiceDelegate(val svc: UserServiceClient) : UserService {
    val roles = "internal"

    override fun userDetails(id: String): User? {
        return svc.userDetails(roles, id)
    }

    override fun createNew(): User {
        return svc.createNew(roles)
    }

    override fun register(currentUserId: String, identity: Identity): User {
        return svc.register(roles, currentUserId, identity)
    }

    override fun findByIdentity(sub: String): User? {
        try {
            return svc.findByIdentity(roles, sub)
        } catch(e: Exception) {
            if (e is FeignException && e.status() == 404) {
                return null
            }
            throw e
        }
    }

}
