package com.akolov.currigate

data class UserPreferencies(
        var acceptsCookies: Boolean = false
)

data class User(val id: String, val identity: Identity?, val prefs: UserPreferencies) {
    constructor () : this("", null, UserPreferencies())
}

data class Identity(val sub: String,
                    val name: String,
                    val givenName: String,
                    val familyName: String,
                    val profile: String,
                    val picture: String,
                    val email: String,
                    val gender: String,
                    val locale: String) {
    constructor () : this("", "", "", "", "", "", "", "", "")
}
