package com.akolov.currigate

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.cloud.netflix.feign.EnableFeignClients
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component
import org.springframework.web.filter.GenericFilterBean
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


fun main(args: Array<String>) {
    SpringApplication.run(CurriGateApplication::class.java)
}

@SpringBootApplication
@EnableFeignClients
open class CurriGateApplication() {


    @Bean
    open fun jwt(): Jwt {
        return Jwt("secret key here")
    }

    @Bean
    open fun registerIdentityFilter(): FilterRegistrationBean {
        val registration = FilterRegistrationBean()
        registration.filter = identityFilter
        registration.urlPatterns = Arrays.asList("/*")
        return registration
    }

    @Autowired
    var identityFilter: IdentityFilter? = null
}

@Component
class IdentityFilter(@Autowired val jwt: Jwt, @Autowired val userService: UserServiceDelegate) : GenericFilterBean() {


    val log = LoggerFactory.getLogger(IdentityFilter::class.java)

    companion object {
        val ATTR_USER: String = "curri-user"
    }

    val COOKIE_NAME: String = "curri"

    fun updateUserInRequest(request: HttpServletRequest, response: HttpServletResponse, user: User) {
        log.debug("assigning new user ${user.id}")
        request.setAttribute(ATTR_USER, user)

        log.debug("setting cookie in response ")
        val newCookie = Cookie(COOKIE_NAME, jwt.create(user))
        newCookie.maxAge = 30 * 24 * 60 * 60
        newCookie.path = "/"
        response.addCookie(newCookie as Cookie?)
    }


    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {

        val httpRequest: HttpServletRequest = request as HttpServletRequest
        val servletPath = httpRequest.servletPath

        val httpResponse: HttpServletResponse = response as HttpServletResponse

        log.debug("processing request $servletPath")
        val cookie: Cookie? = httpRequest.cookies?.find { c -> c.name == COOKIE_NAME }
        val userInRequest = if (cookie == null) null else jwt.getUser(cookie.value)
        if (userInRequest == null) {
            log.debug("no cookie with valid user in request")
            val user = userService.createNew()
            updateUserInRequest(request, httpResponse, user)
        } else {
            httpRequest.setAttribute(ATTR_USER, userInRequest)
            log.debug("cookie found in request: user ${userInRequest.id}")
        }

//        response.setHeader("Access-Control-Allow-Origin", "*")
        chain.doFilter(request, response)

    }
}
